---
title: "Commissions"
date: 2020-05-19T21:35:10-05:00
draft: false
tags: [art]
featured_image: "/images/isaiah-tiev-hero.jpg"
aliases: ["/#commissions"]
---

I offer two different setups for my commissions.

Either way, you're going to want my commission info pdf. It's got all the info you'll need for both types of commissions. Download it first.[^footnote]

* [Commission Info Packet](https://www.dropbox.com/s/0en5oj3dn7a7xtu/Commissions%20by%20Joe%20Bush%20Arcandio.pdf?dl=1)
* [Commission Backlog](https://gitlab.com/voidspiral/FreelanceProjects/-/boards/1049690)

## Standard Commissions
These are fixed-price fixed-feature single-character commissions. Fast, easy, and inexpensive.

* [Etsy Store](https://www.etsy.com/shop/VoidspiralArt)

## Hourly Commissions
These are completely up to you. Price is based on complexity. Customizable to your heart's content.

* [Paypal](https://www.paypal.me/voidspiral)

---

I usually stream my commissions on [twitch](https://www.twitch.tv/arcandioart). Most of the time the streams are from about 8:30 am CST to 10 am.

[^footnote]: I'm also in the process of converting my commission info packet into a page on this website, but it's difficult to figure out how to generate the document from the webpage.