---
title: "About"
date: 2020-05-19T21:35:10-05:00
draft: false
tags: [self]
featured_image: "/images/DSC00286-hero.jpg"
---

I'm a strong proponent of continued learning, creativity, automation, personal data control and security, a detractor of vendor lock-in and abusive corporations. I'm opinionated about software. I think that we need more love in the world these days. If I had a personal social motto, it'd be "Don't be a dick."

I'm a self-taught programmer and I can write html, javascript, css, C#, Python, a bit of PHP, whatever you call Hugo's templating system, and can pick up languages fairly easily. I use scripting to solve pretty much every problem I come across in my daily work. But more importantly, it's taught me to think even more analytically than before, which in turn helps every other facet of my life.

I've worked in the video game industry as a 3d artist and scripter and in software design as a UX/UI designer, both of which influence my way of thinking. I was a philosophy minor[^1] and I can be pretty pedantic sometimes, but I very much cherish clear and critical thinking.

I'm basically synonymous with [Arcandio](https://arcandio.com) and [Voidspiral](https://voidspiral.com). If you see those on the internet, they're likely me.

I collect hobbies. Besides tabletop rpgs and video games, I like to bike, hike, camp, ski, read, cook, eat, and sketch, and I'm into astronomy, archery, outdoor skills, photography, and music. I'm very slowly learning HAM radio, guitar, music theory, information theory, poker, ontology, and vehicle maintenance.

I have a lovely family, an unknown number freshwater fish, 2 bizarre cats, and one sockpuppet of a snake.

I consider table-top role-playing games to be the highest form of entertainment, because they blend artistic expression, the innate human need for storytelling, and the exercise for your imagination.

I'm bad at social media.

[^1]: I know.