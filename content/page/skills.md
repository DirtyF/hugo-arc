---
title: "Skills"
date: 2020-05-19T21:35:10-05:00
draft: false
tags: [self]
type: "skills"

summary: "ARTWORK 3d ★★★☆☆ anime ★★★☆☆ art direction ★★★☆☆ character design ★★★☆☆ critique ★★★★☆"

skills:
  artwork:
    drawing: 3
    lighting: 3
    character design: 3
    portraiture: 3
    environments: 2
    anime style: 3
    realistic style: 2
    stylized: 3
    fashion: 2
    expression: 2
    gesture: 3
    critique: 4
    art direction: 3
    3d: 3
  writing:
    structure: 4
    editing: 2
    character: 4
    technical writing: 4
  game design:
    system design: 4
    mechanic design: 4
    worldbuilding: 5
    hooks: 4
    adventures: 2
    level design: 2
    spreadsheets: 5
  software development:
    C#: 4
    Python: 3
    Webdev: 3
    UCD: 4
    UX/UI: 3
    scripting: 4
    unity: 3
  organization:
    project management: 4
    problem solving: 4
    planning: 3
    workflow setup: 4
    efficiency: 4
    communication: 5
  graphic design:
    typography: 3
    icons/logos: 3
    branding: 2
    game branding: 3
    page layout: 4
    book printing: 4
    document generation: 4
    
---

---

Preferred items are <mark>marked</mark>.

## Software Technologies

* <mark>Unity & Mono</mark>
* WPF/XAML
* Node.js
	* Electron
* Hugo
* PyQT
* Pandoc
* PrinceXML
* Git, Github/<mark>Gitlab</mark>, gitbash
* CI/CD

## Art

* <mark>Clip Studio Paint</mark>
* Corel Painter
* Krita
* Leondardo
* Paint Tool Sai[^1]

## Design

* <mark>Affinity Designer</mark>
* Affinity Photo
* Affinity Publisher
* Adobe Photoshop
* Adobe Illustrator
* Adobe Indesign
* <mark>Figma</mark>

## 3d

* <mark>Blender</mark>
* Zbrush
* Autodesk 3ds Max
* <mark>Substance Painter</mark>
* Cura Slicer
* Ender 3 Pro

### gimmies

* office software & google docs
* <mark>markdown</mark>, yaml, toml, json, xml
* scrivener, <mark>typora</mark>, etc
* command line, powershell, bash, terminal, w/e
* <mark>windows[^2]</mark>, mac, linux
* outlining, flow-charting, brainstorming

[^1]: And I even paid for it too!
[^2]: Yes, even for graphic design, fite me