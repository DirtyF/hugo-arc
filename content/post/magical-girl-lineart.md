---
title: "Magical Girl Lineart"
date: 2019-01-24T10:57:34-05:00
tags: [art, sketch, wip, weakness. lineart]
draft: false
featured_image: "/images/solarian-lineart.jpg"
hidehero: true
---

I'm not great at lineart. When I draw my sketches, there's an expressiveness to them that I really like in the lineart, but those lines are never good enough to use in the finished piece. That expressiveness is lost when I refine them, particularly if I'm doing cartoon or manga style solid black lineart. I'm not great at varying the line weight on those sorts of projects. Here's a comparison.

![magical girl sketch example](/images/solarian-sketch.jpg)
![magical girl lineart example](/images/solarian-lineart.jpg)

Now on one hand, it's sort of a false comparison here, because the goal of this piece is to emulate the style of a detailed color manga art page, so the lineart is *extremely* thin, especially for my work, but you can still pretty much see the difference in line thickness and expressiveness.

I don't know how to fix it yet, but I guess I can add it to my backlog of weaknesses.