---
title: "The Lazy Dm and Me"
date: 2019-03-15T07:27:55-05:00
tags: [game-design, tabletop-rpgs, blog]
draft: false
featured_image: ""
hidehero: true
---

I recently finished Sly Flourish's The Lazy DM. This isn't a review so much as some reflections and a bit of unasked advice.

<!--more-->

Though I was introduced to #ttrpgs through D&D, I didn't have the same "upbringing" that a lot of other 2nd Ed grognards had. Yes, D&D was my first rpg, but in reality we weren't exactly playing *D&D*, not at first at least.

## Story time

I'll keep it short.

My best friend and I had a mutual friend who lived close to our school, so after school we'd go hang out with him. I think we were like 12 or something at the time, this would have been around '97 or so I guess? Maybe? Anyway, the point is that we were young and green. We played a session (or two? long a go it was, yes) of AD&D with this friend, and we had frankly no fucking clue what we were doing, but it was a-ma-zing. I'm not even sure we were actually playing D&D with him either, because we spent most of our time pretending with Lego, and it's not clear to me where one ended and the other began. Anyway, that part doesn't really count, because I don't think either one of us really knew we were playing *a game* rather than just *playing.*

Well, at some point, we borrowed "the book" from him and took it home because it was so cool. We made up our own rules and played our own games, because we had no idea how to do it right.

It turns out we had the 2nd Ed Monster Manual. So yeah, not exactly the best place to start playing the game.

Eventually we got the rest of the core books, of course, and we must have transitioned rapidly over to "real" AD&D because I don't remember much of the rules we made up. We got pretty deeply invested and I started a collection of books that would eventually drive me into [full-time table-top game design](https://voidspiral.com).

But there was one important thing missing from that collection: adventure modules. To my recollection, we never had *any* of them. We focused exclusively on character options, expansions, and monsters. I don't *know* why we never got any adventure modules; it could have been that they just weren't popular enough to find a place on the shelves at the Waldenbooks in our mall (RIP Waldenbooks, RIP mall) or it could have been that we just didn't care.

Point being, we were on our own. I was mostly the DM, and my ideas came from all over the place, shamelessly ripped off from books, games, movies, art, magic cards, and just about everything else. I had to design and run those games myself, from my own head, and I had to learn to take notes and plan campaigns from the DMG. I never learned how to use or stick to a pre-defined plan the way you need to with an adventure module.

## The Unclear Transition

I know that I liked planning out campaigns, and I know that occasionally I did so with great detail, the way one might design a mega-dungeon or an entire complete sandbox. But by the time I got to college, my design philosophy was already fairly solidified, and it wasn't focused on detailing out the minutia of encounters and building elaborate quest chains. It was focused on characters, villains, interactions, events outside the player's control, and parceling out information.

## The Dry Spell

College happened, grad school happened. We moved to Texas, then to Illinois, and with each step I lost friends I'd gamed with for years. Slowly, the experience I'd built up from GMing and DMing for decades ebbed away and the blade rusted in the sheath. We didn't play a lot of D&D for a long while, and I didn't even play many other RPGs for a while either. When I started running Voidspiral full time, I already felt rusty, inexperienced, and sluggish. It took a lot of work for me to start building back up my skills.

Eventually 4th Ed hit, and while I liked the system as a system, I didn't like how constrained it felt compared to 3.5e. Obviously, I didn't play a lot of it, but that feeling suggests that part of my perception of GMing had *drastically* changed. I wouldn't have even thought twice about reskinning a race or designing a new one in college, and her I was a few years later afraid to even start tinkering.

Maybe it was grad school, where it could be argued that I *actually* learned to design games, or maybe it was my lack of practice. I don't know. What I do know is that I *still* didn't get back into D&D until years later again, round about 2018.

## Crisis in Infinite Joes

Back in the recent past, as I rejoined D&D society, I started to feel constrained all over again. It seemed like D&D was played closer to the book and firmer to the rules than I'd basically ever had before, and I'd played with some *truly amazing* powergamers in college. Confidence in pieces, I started looking for ways to step my game up.

## Finally, back around to the beginning

So I got The Lazy DM and read it on my Kindle. Which was good, I highly recommend the book to *everyone* who plays RPGs, not just seat-of-the-pantsers or D&D players, but *everyone*. You don't even have to apply it, obviously, but having it in your head will help you and make the game more fun for your players.

Why?

The gist of The Lazy DM is this.

> Overplanning makes you railroad your players in order to stick to the plan. Railroading bad. Instead, prepare the *absolute minimum* amount of content to run the game. We're talking 3 places, NPCs, villains, and encounters, and that's basically it.

Which I like, a lot. I like how concise and clear that makes the issue: the more you prepare a campaign, the less options the players have. Focus your time on the parts that *need* to be done, and no more.

The book goes into detail about how to do this and why, of course, but this isn't really a review. I recommend you go read it if you haven't.

## The Lost Art

It turns out that I'd literally forgotten how to DM. Somehow, modern D&D tricked me into thinking that I *had* to design a campaign, and that there's no way that I could get by "winging it."

Which is hilarious because that's exactly how all of my greatest campaigns came about. I didn't go in with a vast and enormous setting bible, a detailed map of every encounter, and a schedule, I went in with blind feelings, suggestions, and an outline.

*That's how I roll.*

I mentioned the key points before, but let me address them more specifically.

**Characters:** USE THE BACKSTORY LUKE. For the love of god, if you don't use the PC's backstories in constructing your plot, I swear I will come to your game and *switch all your dice for ones weighted for crit failures.* Players like their characters. It's meaningful to them. USE IT. What the hell kind of story is NOT about *the characters in the story?* Arg. Rant over.

No one wants to play second fiddle either. Make the PCs the MAIN CHARACTERS, please, not a side-story.

**Villains:** You need good bad guys. Like really good ones. Brilliant villains, ones that will live in infamy with your players for all eternity. I've managed a few of these, but not many. They need to be at least as well defined as the PCs, so make that where you put your heart and soul. Just remember that they're not the spotlight, they're the *an*tagonist, not the *pro*tagonist.

Your villains might not actually be *villains* per se, but you know what I mean. Antagonist. Opposition. Enemy. Whatever.

**Interactions:** Don't design with encounters in mind, design with *relationships.* Figure out who thinks what, and who feels what about whom, then weaponize that. To do this, you need to focus on the characters, NPCs, and antagonists. Here's the cool part: this shit is independent of level, and even of mechanics. You don't need to know the HP or attack bonus of the adventuring guild master to know that they're secretly trying to get most of the PCs killed so that they can save the one they like and take them into their personal cadre. Or whatever. Make a flowchart, and figure out how everything fits together. Make a bunch of relationships that the PCs know nothing about. That way, when those characters do stuff, it'll feel *real.*

Don't forget to consider what the characters are aware of. Design some relationships that don't exist yet and some that are no longer valid. Don't let your web be static. Come up with some potentialities, even if they never come to pass.

**Events outside the player's control:** This is my trick for not railroading PCs. If you really want something to happen in the story or you have a big thing you need to reveal, put it out of the PC's reach. I don't mean this in a cruel or "us-vs-them" way, I mean that the PCs naturally have limited influence in the world, and events from outside their sphere of influence can make excellent prompts for adventures. In one game, the PCs came back to their home town to visit a childhood friend. It turned out she'd already gone missing. Because they've arrived too late to stop it, I don't have to worry about the PCs getting in over their heads with the dragon that kidnapped her, or screwing with the core conflict of the story before the story even occurs. Events like this happen all the time in fiction: Stormtroopers killing the Owen and Beru Lars, for example. This doesn't have to be about loved ones, either. It can be rumors of great dangers or the fallout of the adversary's activities. Done well, it makes the PCs feel like things are happening out in the world. Done poorly, it makes the PCs feel like the GM is just yanking their chain.

**Parceling out information:** You're the GM. You're the player's eyes and ears in the game world. They literally cannot see or hear anything that you don't tell them. They can't pursue actions that they can't imagine, so use this to your advantage. You can withhold information that you don't want them to know and give out information you want them to pursue. Do *not* just make them bang their heads against the wall trying to figure out where to go or what to do. Give them a freaking clue. Withholding all the information doesn't make the game mysterious, it makes it infuriating, or worse: **boring.** Do you want to play or don't you?

My style employs this judiciously at the micro-level as well. You can *guide the eye* simply by putting more detail into your descriptions of certain things. Conversely, if you don't want them wandering all over hell, don't give them detailed descriptions of every red herring you can imagine in the scene. That way lies madness. And pure chaos.

## To summarize

* Be PC-Focused
* Design Interesting Adversaries
* Plan with Relationships
* Prompt PCs With Events Outside Their Control
* Control The Flow of Info
