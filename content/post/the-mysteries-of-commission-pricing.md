---
title: "The Mysteries of Commission Pricing"
date: 2019-04-18T11:05:25-05:00
tags: [blog, art]
draft: false
featured_image: ""
hidehero: true
---

*I mean that they're mysteries to __me__, not that I know enough to reveal them for you. Questions, not answers.*

> Before we get underway, my commission info packet / menu is [here](https://arcandio.com/#commissions). You may want to refer to it throughout the post. And if you happen to actually want a commission, let me know!

So I spent a lot of this afternoon analyzing what little business strategy I have, and decided along the way that I wanted to try expanding where I promote my artwork, as a start.

That led me to some of the better subreddits, among them /r/DnD, [/r/characterdrawing](https://www.reddit.com/r/characterdrawing/). But what I found with the latter was that it's strictly pro-bono, which doesn't help me much, and as with other communities, solicitation is pretty strictly limited on /r/DnD.

That's how I found [/r/fantasyartists](https://www.reddit.com/r/fantasyartists), which is specifically created for artists looking for work and clients looking for artists. Reading their rules, I found that according to them, I'm likely underselling the work and effort I put in on my commission work, not to mention that doing so prevents me from posting there *at all.* I understand, subreddits are hard to police, and a lot of communities have to have very strict rules in order to keep the place in order. I'm not faulting them specifically of course. (Though I've had mostly bad experiences with actually trying to promote my work and show people stuff on reddit, frequently due to over-aggressive rules and policies)

My problem is that the thing threw my whole carefully crafted pricing model into question.

# Straight Talk

*I'm gonna talk about actual numbers now, so if that makes you uncomfortable as corporate life has made many people, skip to the next heading.*

According to /r/fantasyartists, the standard/minimum rate for fantasy art is a minimum of $100, and an hourly rate of $30.

My commissions range from $20 sketches to $80 portraits, and into the multi-hundreds for what I call "custom commissions," which just means it doesn't fit into one of the very small boxes I use for those inexpensive commissions above. I've charged /r/fantasyartists prices for a lot of my commissions, but I've also sold a lot for cheaper.

Now, one problem that I've got is that I don't charge for $30/hour. I've seen my commissions take from 2.5 to 7 hours. Granted, the 7 hour piece was one of the more expensive custom commissions, but some of the ones I've sold for $60 have taken me 5-6 hours, so they should have cost more like $150-$180. So that's not great, but it's not easy to charge people hourly on Etsy, where most of my commissions are coming from. It seems to me that the market I'm selling to wants up-front pricing. Which brings me to my next point.

# Separate Markets

I think I'm not selling to the people that /r/fantasyartists are selling to. Though if the talk on the discord is correct, they're not selling much at all through the subreddit. I'm selling these pieces exclusively so far to people who want to either commemorate gaming experiences, or to people who want to go "all in" on an upcoming gaming experience. They're civilians. Non-game-designers. These aren't commercial clients, they're personal. And I don't think that personal clients have the kind of cash to throw around for me to charge $30 an hour, unless I suddenly get hella-fast.

Now, I would **love** to get some business from commercial clients, and charge them an hourly rate, (which I just added to my commission menu) but:

* I don't know how to find those clients
* I'm not good enough to be anything but "middling" at the moment
* I can't afford to change over pricing structure completely, because then I'd have to abandon Etsy and I'd end up with zero clients

So what's an artist to do? I've slowly started ratcheting up my prices because I've started to produce some better work, but I don't want to scare off the clients that I'm already getting.

To be blunt, I'm not interested in fooling around with /r/fantasyartists at this point, because after I did all this thinking I realized that the subreddit is actually fairly tiny and hardly anyone's getting business from it. It's not worth the effort to post there on top of everything else I'm trying to do to promote myself on social media.

I don't have answers for these questions right now. It's just frustrating. 