---
title: "Computer Color Is Broken"
date: 2019-03-25T10:32:37-05:00
tags: [blog, rant, software]
draft: false
featured_image: ""
hidehero: true
---

*Another art software rant*


This is the title of a [MinutePhysics video](https://www.youtube.com/watch?v=LKnqECcg6Gw&feature=youtu.be) that talks about how most color blending done on computers is mathematically incorrect, leading to ugly, muddy colors when blended together. You can watch the video if you want the specific math (which is pretty simple and easy to understand) but the problem itself remains: *Color blending in essentially all the software that I use to paint is horrifically wrong.†*

†Caveat: I *can* work with correct color, but only in huge 32 bit files in Adobe #Photoshop, which is my last choice for painting, after #ClipStudioPaint and #CorelPainter.

## The Way Forward?

So what am I supposed to do? What do other artists do? Plenty of artists I admire can paint with convincing color and seem to have no problem mixing good colors. To be honest, I'm not sure I've ever noticed this problem in actual usage, but I do seem to often think that my colors aren't very good.

The other problem is that the fix mentioned in the video is one for *the software engineers making the software* not us mere mortals trying to *use* the software. I can guarantee that even if Corel implemented a fix, I'd still have to pay another $200 for an updated version of painter. We can all basically write off the "easy fix" suggested by Minute Physics.

The alternative is actually sort of encouraging in a hard, annoyingly art school sort of way: ''Don't blend that way.''

The techniques I've seen a lot of digital painters employ are from people who've been doing it for a long time. Those folks probably started before there even were decent lag-free blending brushes, so they learned to mix colors by sampling: they use the Paint And Sample methodology. They paint with transparent colors, then sample, then paint, over and over again. For new colors, they either sample from a palette (which seems more rare) or they simply do direct color adjustments from the Color Picker.

These artists can do this because *they know better color theory than the computer.* They know that mixing red and green should give you a nice yellow instead of an ugly brown. Then again, maybe I'm looking at this the wrong way. Neverminding the reasons for the software mixing to work the way it does, maybe it's roughly equivalent to the way an oil painter actually works. Pigments are not light, so when you mix alizarin crimson and viridian green, you don't get lemon yellow you get a dark graybrown mess, not unlike when you mix #FF0000 and #00FF00.

Point being: you've got to pick more colors, and not rely on blending for full color transitions. Add or modify the hues in your shadows, lights, and gradations. Be mindful of your colors.