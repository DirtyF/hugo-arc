---
title: "Fate Core Common Complaints"
date: 2019-03-05T11:50:16-05:00
tags: [blog, tabletop-rpgs, fate-core, game-design]
draft: false
featured_image: ""
hidehero: true
---

Fate gets a lot of hate on social media. Some people hate it, some people love it. As a designer, I want to know what the issues are, so that I can avoid them in systems I design, and patch them in games that I write using Fate.

<!--more-->

Some of this is inevitably going to sound defensive, but I'll try to not play fanboy more than I have to. I'm genuinely interested in the concerns, because you can't improve something without understanding the flaws.

After trying to pack this all into a single post, I've decided to break it up. You can find more of these in #fatecore.

# Too freeform

Starting with one that's pretty easy to refute. A lot of people say that Fate is too freeform. I can understand that it might *feel* that way in comparison to some games, but it's just not. Fate Core as a system clearly defines what you can and cannot do as a player. Each of your skills has particular use cases, and stunts merely modify what you can do with those skills. Even vague aspects are codified: you can invoke or compel them, you don't get to just take over because your aspect says "god" or something. You *can* make use of them to inject your own details into a story, but that too is codified, and costs a fate point.

**Declare a story detail** is one of the few weak points with this. The book *suggests* that your DaSD should relate to one of your aspects, but doesn't require this. In my games, it's required.

Now, there are no *explicit* limits on what you can use a DaSD to do, but I mean, it's pretty obvious that it should be a *detail* and not a major overriding factor. You could say "Oh, sure I have lockpicks on me," but not "Oh, well, I can walk through walls," or "I pay a fate point for them not to lock me up." It's "a detail that works to your character's advantage."

Other specific complaints in this category generally fall into the same type of response. They don't provide a lot of explicit limits to things, instead relying on the GM to have a good head on their shoulders and keep things from getting out of hand. That's not "too freeform," it's just erring on the side of rulings instead of rules. You can also consider this pragmatically necessary since Fate Core is too generic to provide explicit rules for every eventuality.

More Fate Core issues, analysis, and defense to come.