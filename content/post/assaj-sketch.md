---
title: "Assaj Sketch"
date: 2020-06-05T09:10:06-05:00
tags: [art, sketch, wip, blog]
draft: false
featured_image: "/images/assaj-sketch-hero.png"
hidehero: true
---

![sketch of an anole lizardfolk dancer](/images/assaj-sketch.png)

Felt pretty good about this one so far. Took about 40 minutes, and didn't have any major hiccups in the initial drawing. Excellent reference; the client did a great little draw-over on a reference photo of the pose and snakes and scarves.

My hands are still sore from all the [deck work](/post/rebuilding-the-deck), but it seems like while that makes it hard to work for long periods, it really forces me to draw lightly and loosely, which is good. Smooth drawing was perfect for this character.

Too bad the scope is so close, because I drew some very nice legs for her, but they're cropped out now. Such is life.