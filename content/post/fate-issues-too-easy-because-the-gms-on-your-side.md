---
title: "Fate Issues: Too Easy Because the GM's on Your Side"
date: 2019-03-29T10:42:08-05:00
tags: [fate-core, blog, tabletop-rpgs, game-design]
draft: false
featured_image: ""
hidehero: true
---

*A continuing series on the #CommonComplaints of [#FateCore](/tags/fate-core)*

This is true if the GM's not interested in making the story actually challenging for the PCs. The GM *does* need to apply pressure on the characters of the story, because the system itself isn't going to do that. Not like a D&D adventure module is specifically composed of threats and dangers. The GM needs to be able to think of and present decent challenges for the PCs. It's the same mindset, but a different execution.

Now, if your GM is consistently not challenging the PCs, that's a problem with the group and the execution, rather than the system, and that actually makes it easier to fix: just say "Hey, can we take on some tougher challenges?"

It's also important to remember that in Fate, the GM's job is to help tell a *compelling* story, not an *easy* one. They're on the side of the Players, who value entertainment, not on the side of the PCs, who value success. If it's going to be more entertaining for them to fail or be challenged or have to make the hard choice, *that's* what they ought to be doing.