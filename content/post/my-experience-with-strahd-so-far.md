---
title: "My Experience With Strahd So Far"
date: 2019-02-17T10:40:10-05:00
tags: [blog, tabletop-rpgs, rant]
draft: false
featured_image: ""
hidehero: false
---

> **Massive disclaimer time.**
> 
> * Content warning: Spoilers, philosophy of RPGs. Expect disagreements of schools of thought.
> * I do not fault my DM.
> * Yes, I'm butthurt and salty.
> * If one of these issues *is* caused by my DM, I don't intend offense, *it's clearly just a difference of play style vs DM style.*
> * This is probably mostly a rant.
> * I have not read the Curse of Strahd, so I'll mostly <strike>be talking out of my ass</strike> be talking about my experience as a player. Also, to prevent me from getting spoilers, I've also tried to avoid reading about it as much as I could.
> * DM: If you're reading this, take everything with a grain of salt, and **do not take it personally.** Reading this kind of post from my players *or even just on the web* would give *me* anxiety attacks, so put it down if you need to. We can talk about it directly if you want.
> * I've not yet done my due diligence on Player Agency to see what others have to say about it, but as a writer I've got some pretty strong opinions of my own.
> * I don't like bashing/ragging/ranting about stuff. I believe that to make the world a better place, we should focus on the positive. I'll try to do that, but it's not exactly the point of this post.
> 
> Spoiler Alert: Curse of Strahd, Death House Spoilers


I recently moved back to Wisconsin, where most of my friends still live. I've lived away for a long while, and while away it was always harder to organize people for some #ttrpg action than it was when we were together and could visit one another. My wife and I play a lot of games with each other, but it's nice to A: both be players for once, and B: play with some other people. (Additionally, it's very hard to be a game designer when you can't play games as often as you'd like.)

The first game I came back to in Madison was a friend's Tal'Dorei campaign, which I sort of inserted myself into, even though I had no idea what was happening, because (I admit) I'm not into Critical Role the way a lot of people are. That game went for a little while, but eventually folded up due to other commitments. (Damn adulthood stuff, grumble grumble neverland etc.)

The next game I was invited to was a game of Curse of Strahd. It was pitched as a bit meat-grindery, which I thought would be fine, and it was suggested to us that we have at least and idea of a spare character in case of character death.

Now, my thoughts on character death in *normal* D&D games aside, that set the tone, as did the mention of our first adventure arc "The Death House." I thought I was ready for some serious meat grinder action, kicking it old school style. I didn't stop at character ideas, I wrote up a family, with each character having a reason to come looking for the last, should they die, and did full character sheets for each, safely tucked in my folder for the campaign.

I played Balethia Wod, a sorceress more or less pretending to be a rogue. I had a light crossbow and crossbow master, and was aimed at being able to rack up the damage. Balethia was a shadow sorceress, which I thought would be fun since the idea was that we ended up somewhere in the shadowfell.

**Content Warning: Abuse**

> Balethia's family was persecuted for her natural magical talents. Her parents sided with society, and abused her.

Her elder brother and sister ran away with her and the three of them became adventurers. The elder two were by backup PCs. Elavigen Wod, the sister and a Sun Soul Monk, was protective of Balethia, but somewhat too focused on discipline. Zalgus, the eldest brother and a Paladin, had no problems with magic, but was Master of the Long Suffering Sigh.

Yadda Yadda, the Vistani come and convince us to go with them to the land of Barovia. We go, our party consisting of a warlock, ranger, grave domain cleric, and Belethia, the roguish shadow sorceress.

## The Death House

**MAJOR SPOILERS AHEAD YOU WERE WARNED**

The first thing we see when we come in is the two crying kids. They need us to look for their parents in their creepy old house.

At this point, I'm thinking, well, we're adventurers. Let's stir up some trouble. I'd set up Balethia to have a thing about kids and parents and family, so that allows me to hook her in. But at the same time, I know this isn't a great idea. But what can we do? We're playing D&D, we can't really refuse the call to adventure. And aside, to do so would be role-playing extreme callousness, which is sort of like one of those terrible buttons on a website that gives you the options:

* Sign Up With Your Credit Card
* No thanks, I hate being smart and cool and awesome

I don't know if I agree with calling this kind of hook "cheap drama," but that's sort of how it felt after the fact. But I mean, what other hooks can be used? We're already getting into the territory where many rational people, adventurer or not, would simply be like, "Hard pass. Looks like a trap, and we've got no skin in that game. We've got things to do, as well."

But no, even *while shaking our heads as players* our PCs shuffle suspiciously inside the house.

We do alright, to be fair. We stumble across some unknown percentage of the content available to us in there, and we do ok at it. We take on a ghost I think, some kind of zombies I think (this was a while back, sorry for the lack of detail. I did get the impression this was all pretty much right from the book as written, so that may correct me) and fought a shambler plant thing. Because it'd woken up when we disturbed the rotten plant matter, when the DM showed us a bigger pile of plant matter later, in the crypt(?) I was immediately like "hey guys, get ready for a big fight," and we took a moment to try to set things up so we'd have an edge. To their credit, the GM didn't telegraph it any more than that. I guess we did pretty good, because we technically killed the shambler without any deaths. Until the end, when our grave domain cleric bought it because of poison or something, which happened right after the critter went down. (Full disclosure, the Cleric is played by my wife. More on that later.)

Note: I don't blame the DM for that character death. As far as I can tell, it's just how the dice fell. But in retrospect, that monster was ridiculous for our level. I'll gladly side with the DM that it was all by the book, but...

My memories of the timing of things are hazy, but I can at least assume that our escape attempt started after that fight. But I do distinctly remember two oppositions to our escape:

1. We found a window with a balcony. It lead to nothing but swirling mist, and it was strongly implied that we couldn't get out that way. Okay, that's weird, but whatever. Obviously this place is haunted.

2. Once we'd fallen and broken through a few rotting walls, I got the idea to do a good ol [dungeon bypass](https://tvtropes.org/pmwiki/pmwiki.php/Main/DungeonBypass) by just smashing through the floors and walls until we made it back to the exit. This didn't work because apparently the floors are invincible.

Eventually, we made out, minus one. I forget how, but we made it out of the house with the cleric's body. Balethia set to work giving her a proper funeral, using the cleric's own book and will (Grave Domain Clerics are prepared, it seems). Oh, and (SPOILERS) the house was haunted and the kids were dead. Nothing was gained except a cool hat and some experience, and "tone setting."

I need to talk at least briefly about what this did to Balethia: it destroyed her. I was deep into that character, and spent a *lot* of that game role-playing her very detailed issues with rich people, parental abandonment, and problems becoming close to people. So when one of the only people she'd finally gotten close to died in what was essentially a meaningless battle that had nothing to do with our (very abstract) mission.

I wasn't having a lot of fun with that. It was very exciting to be sure, but in a "roll the dice to see if you die" kind of way. The whole thing at that point seemed less of D&D and more of a rails shooter or casino gamble. And that's because at several key points, our **player agency** was taken away.

*Again, let me stress, I believe this to be a problem with the book, not my DM.*

## Player Agency

Getting dragged into the Death House, being trapped there, being trapped in various fights, finding out that some things are patently invincible or impossible, these deprived us of agency. Now, some of them were *in the book* while others were *our choices,* but even the choices we made were basically constrained to moving us through the adventure module.

> arcandio climbs on an unearned soap box

Here are three things I believe:

1. **The players should be allowed to decide what their characters do.** It's not a game if the players aren't the ones making the choices.
2. **I *want* players to creatively solve encounters and challenges in my games.** I like to reward them *more* for that kind of behavior, because it's awesome, clever, and fun. Now, if they haven't *really* solved the problem, they may and up having to deal with the issue again later, but that's literally *organically-grown foreshadowing* that makes the story better that I didn't even need to prepare.
3. **If rules exist in the system for something, *never* throw them out the window just to make things harder.** Find ways that the *players understand* to make things harder. Telegraph the difficulty, and let them actually attempt things that they want to do. Give them a chance.

That pretty much sums up the issues I have, not just with the Death House, but also with the rest of the adventure module, and a lot of other games besides.

### What Happened to Balethia

Balethia left the party to deliver the cleric's ashes to her home temple. It was strongly implied that she was killed along the way. I don't like that, I guess it's out of my control, which seems to be related to the core problem.

I made a new PC specifically designed to play in Curse of Strahd, one who has hooks into the story, is a Barovian herself, and is built for combat. Unlike Balethia, who couldn't possibly want to continue with that adventure after The Death House, Carmilla has motivation and means to go after Strahd. A better choice in the game, but I don't know how I feel about being forced to make a character specifically for an adventure module, to "play it right."

## Later On

This post is already getting long in the tooth, so I'm going to skim over some more related issues.

* My new (local) PC ate a pastry from a woman in the town, failed one save, and was basically useless for a day or so. I tried to roll with it, but it was another frustrating example of the "oops, you triggered a trap, you're hosed" mentality, apparently built right into the module.
* We fought Strahd himself at level 3. While I get the "show off the villain" kind of thing to show that they're a threat, I don't think anyone actually likes the [Unwinnable Boss Fight](https://tvtropes.org/pmwiki/pmwiki.php/Main/HopelessBossFight) trope.
  * His powers were explicitly and specifically written to exclude the normal methods for overcoming regeneration, the ones that make complete sense. I get that there's some kind of cosmic mystery around that, but it still falls contrary to belief #3 above. Probably, he had plenty of HP regardless of his regeneration.
  * He had specifically written legendary actions that allowed him to move around and attack without triggering our reactions. This also fits into #3. Those powers may only have existed for that fight, to show off, and will be absent later, in which case it's clearly just artificially increasing the difficulty, or it may be that those are special powers that run rampant over the bread-and-butter abilities of PCs, which is similar to but not *precisely* a #3 problem.

## My Fix-Fic for Strahd Issues

Again, I haven't read the book, so this could all be misunderstanding. And since I haven't read the book, it's foolish of me to suggest ways of improving it on the whole, but here are some ways that might deal with the specifics.

* **Improvisation:** My best games are often the ones that are least over-planned. My usual method is to build a framework of events that happen outside the PC's control (which is pretty wide in my games), with flexible details, character ideas, areas, encounters & all that ready but *not necessary for moving the story forward.* Let the PCs do as they will, and follow *their* story.
* **[Sly Flourish's Method](https://www.amazon.com/Lazy-Dungeon-Master-Michael-Shea-ebook/dp/B00ADV2H8O):** Sly Flourish recommends an even lighter approach to preparation, because of the core tenet that *over-preparation leads to over-investment leads to inflexibility leads to railroading.* Because the Strahd prep is already done (by virtue of there being an entire book prepared for the adventure) you shouldn't feel as over-invested following the plot as handed down, because you didn't spend as much of your time slaving over the details. Let the story follow the players.
* **[On Traps & Cheating](https://www.youtube.com/watch?v=jVeQcDfQq_o):** Yadda yadda social contract, you know the drill. Giving the players no clues at all about potential traps, pitfalls, and bad decisions, is antithetical to "gameplay" because to play a game, you must have information to make a judgement about your choice. In the absence of information about the choice, the choice becomes meaningless. Being punished for meaningless choices is just not a good idea for the player's enjoyment of the game.
* **Telegraph Challenges:** Also talked about in the video above, and partially covered by the choice thing. But also, isn't it more *interesting,* not just for the players, but also for the DM, for players to actually *engage* with challenges? Who is "rocks fall, everyone dies" fun for? Why insert punishment in the middle of business-as-usual? For "mood?" Try giving some exposition instead. Show them some NPCs down on their luck.

If you made it this far, thanks. I feel a little better now. Feel free to rant at me back.