---
title: "A Blog About a Vlog"
date: 2020-02-06T11:15:59-05:00
tags: [blog, video, tabletop-rpgs, joes-table]
draft: false
featured_image: ""
hidehero: true
---


*I just made a youtube video, like LITERALLY EVERYONE ELSE and posted it. Big whoop, right?*

> I should really be doing my taxes right now.

Here it is:

[![joe's table youtube video](https://img.youtube.com/vi/a_UAQbK76sI/0.jpg)]( https://www.youtube.com/watch?v=a_UAQbK76sI)

# how was it?

It was ok. I was nervous for releasing my first "real" youtube video, but really, probably nobody's going to even notice, so what's there to be worried about? I've planned for a few "warm up" videos before I get into the ones I really want to make, so hopefully I can gather a few people who might be interested in watching them by that point. Possibly not though. I'm no Mercer or Colvile.

# what are you going to do?

Seems like there are a lot of production issues on this video.

* I need to get better at recording better takes of entire segments so I have to spend less time crappily editing the video after the fact.
* I got a lapel mic, but it's kinda junky and seems like I should maybe stick to the more expensive shotgun mic
* There seems to be a bit of crackle when the voice track tops out, and I couldn't isolate it. Not sure where it comes from or if it's in the original video.
* Video's not very crisp. Here are some possibilities:
  * focus just isn't dead on. I'm using manual focus and I can't sit and focus at the same time. Possibly try autofocus.
  * Reflection from the teleprompter is screwing up focus. Cover teleprompter better.
  * teleprompter glass or lens is not clean.
* Yes, there's a bright spot in the video from internal reflections in the teleprompter. I learned.
* music is kind of rumbly and possibly annoying to some people. I've got no feedback on this yet.
* White balance is jacked, I know, and I think I've fixed it for the future.
* Background isn't blurry enough. I used FFMPEG to denoise the video's very high ISO, but possibly I need even more light to lower the ISO and get less noisy video while pushing the aperture wider for more depth of field.
