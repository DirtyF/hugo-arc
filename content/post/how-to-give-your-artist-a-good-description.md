---
title: "How to Give Your Artist a Good Description for a Commission"
date: 2019-03-29T10:54:06-05:00
tags: [art, self, blog]
draft: false
featured_image: ""
hidehero: true
---

*I've read a fair few briefs in my time so far, and I'd like to give some advice.*

This is mainly targeted at those who want to [commission me](/page/commissions) personally, but I think that others would also appreciate this sort of thing. Most of these points are aimed at making it easy for the artist to quickly and efficiently find the info they need in the brief while working. This might be more important to those who stream, like myself, than those who can take all the time they want.

* Bullet points are good. Long, rambling paragraphs take time to re-read while I'm working.
* Clear, specific descriptions are best.
    * Purple prose doesn't help much, especially if it's vague. There's no need to be artful in your description, what I need is specificity, accuracy, and consistency.
* Reference images are king. If you've got past commissions, send them along. If you don't, your artist will deeply appreciate it if you send them some found photos that you can use to describe things. Find a picture of a face that's close to how you want your character's to look. This especially helps with clothes and equipment.
    * Even if the reference image isn't perfect, you can always tell me what you *don't* like about it, which is almost as helpful as a perfect reference photo.
    * It's perfectly fine to send other fantasy art or pieces by other artists. It can be especially helpful for things that you won't find normal pictures of.
* Try to keep things organized. Don't jump from hair to garments back to hair to face to socks. Start with the pose, then face and personality, then clothing and details. This is because I sketch a pose and face first, then "equip" the drawing as I refine it.
* If you're using bullet points, put details in sub-bullets.
* Consider that some elements are more important than others. Make note of the important things, and make note if something is just nice-to-have gravy kind of stuff.
* Personality. Tell me what your character is *like.* What kind of stuff are do they do? What are they known for? Even if you're commissioning a T-posed character sheet, this will help with *at least* the expression, if not the whole piece.
* Verbs. What could they be doing in the piece? Think of it like a snapshot. The better snapshop would be while they're in the middle of an action, rather than just standing around.
* Complex stuff is hard to draw.
    * Be aware that you can't see the lower levels of multi-layered clothing.
    * Keep in mind what you paid for. If you bought a single character commission, you'll get push-back if you describe someone else participating in the scene.
    * Really small stuff might not show up well, especially if you want an image that shows the whole character, or it's positioned somewhere it'll be hidden or behind the body.
    * Often artists will leave some things out to help clarify the design. This is especially the case of RPG characters, who tend to be described as carrying much more stuff than would make sense or look good. Again, try to focus on the stuff that's important.
* Body type is a big help, especially if the artist shows a lot of different body types in their work. Keep in mind though that some artists are more comfortable with the body types they do most frequently. Reference here helps a lot too.
* Ask questions if you're unclear on something. I'd much rather chat about something than have you stress out over something you aren't sure about.
* Changes are always easier earlier in the process. Changes aren't necessarily bad, but keep in mind that it can be frustrating to go back over a bunch of tiny things over and over again, and that may take time away from other things.
* If in doubt about something, just ask.

If you've made it this far, thank you sincerely. Following basically *any* of these steps makes my job *so* much easier.

[You can commission me here.](/page/commissions)