---
title: "Deep vs Wide"
date: 2019-03-01T11:46:05-05:00
tags: [blog, self]
draft: false
featured_image: ""
hidehero: true
---

*More unhelpful soul-searching*

This post was inspired by conversations on [Tabletop.Social](https://tabletop.social) with [Alex Schroeder](https://tabletop.social/@kensanata) and [Frotz](https://mastodon.weaponvsac.space/@frotz).

I collect hobbies. I used to think that when I said that I meant it sort of as a badge of honor, that I have wide interests and am a well-rounded person. But I also know that it's all a distraction from everything else.

I pick up new things pretty easily, but I put them down just as easily. Rarely do I dive super deep on a given subject, nor do I ever really attain mastery at anything. I'm sort of a professional hobbyist.

On one hand, that's good for my writing ability: I've got broad enough knowledge that I can fake it part of the time and know how to efficiently look stuff up the other part of the time. I heard somewhere (a youtube video perhaps? TED talk? Or was that a book I abortively sampled on Kindle?) that having a side-project often really helps your creative & problem solving mind, because you can put down a hard problem you're stuck on, clear out the stuck ideas with distraction, then come back to it when you've got a fresh perspective. And I think that's totally valid.

I'm just doing it wrong.

I've wanted to really get good at drawing and painting for... wait, well, decades, now. But more specifically, I've actually put drawing in my day as a practice routine since about a year ago (whenever it was that I started my [sketchblog](https://pixelfed.social/Arcandio)), and while it's been helping, it's easy to see that it's not really doing *that* much to increase the quality of my work. I occasionally take on major practice time, then give up part-way through, when something more pressing comes along.

Sometimes, that's just the nature of the beast: I work for myself, so if something's on fire, ain't nobody gonna put it out but me. But other times, I just... lose the thread. Or give up. Or find something else more interesting.

## Case Study

Take this drawing for example.

[![a series of cartoony expression drawings of 3 characters](https://pixelfed.social/storage/m/586797eda70505ed4fbe3e1ff8acff81afc35f9b/810f263c5bbfcb9f519eb0b689f12e981e52cc4d/ked78oSkigjJ4GHTROQwxAc5oOdHVK20QiNe2fU6.png)](https://pixelfed.social/p/Arcandio/38547)

Apropos of nothing one morning I decided that I'd try tackling expressions as a way to get better at portraiture, since a lot of my [commissions](https://gitlab.com/voidspiral/FreelanceProjects/wikis/commissions) are portraits. I struggled through, because I wasn't that confident with either the cartoony style, or drawing "extreme" (from my point of reference) expressions.

Despite the fact that the sketch is one of my most popular posts on my new sketchblog, I immediately fell off the boat; I didn't continue doing expressions, I didn't continue exploring the cartoony style, I just moved on to something else. And considering that I have 2 different sketch sessions (morning and evening) I must have dropped that like a block of hot lead.

## I'm Wide, not Deep

Saying that I'm like a Bard is kind of diffusing the issue. It obfuscates the fact that I'm *trying* to improve the things that I'm mediocre at. Imagine a Bard that wants to be a Wizard, who keeps seeing how awesome the Sorcerer is.

I'm wide. I apparently find it almost impossible to dive really deep on something, except maybe for scripting or programming tasks, and even there, I'm only good at deep-diving one specific task, rather than teaching myself all the deep core concepts of computer science.

The example of my artwork is only an example, of course. The same thing applies to all of my other skills, I think, even including my ability to design games. I can get fairly deep into that process, but somehow I never seem to get deep enough to see all the angles. I always find something later that someone else has a problem with, often in a pretty deep, meaningful way.

Part of the problem seems to be how easily fascinated I am. I like learning, and I find new things that interest me *all the damn time.*

## I trip easily

When I'm starting some hobby or project that isn't for work, I find it easy to hit stumbling blocks and give up.

* Guitar: not very good at it, can only play with Rocksmith's help.
* Ham Radio: bought an SDR, but haven't used it much because I can't find anything to listen to.
* Concept Art: Oh I've got this book, I can follow along and come up with some decent images. Didn't work the first time? Need more tools? Doesn't look right? Time to move on I guess.
* Painting: A lot of a good painting is a good drawing underneath. If I can learn to draw better, I'll be able to make better paintings. OOPS! Drawing's hard too! Yay!


## Quitter

I want to say that I'm more of a "good enougher," but that's also obfuscating reality. If it's for work, I'll attack that sonofabitch until it's pounded into the ground. But if it's just a hobby thing, or only *related* to work, I find it easy to give up and put my time somewhere else. There's always something else to do, after all.

I do quit. I don't quit on projects very often, unless they're in the early stages and nobody cares about them. But I quit side projects all the time. I quit video games, I quit DMing [#ttrpgs](/tags/tabletop-rpgs) (sorry, honey, promise we can play Starfinder and D&D once my voice gets better), I quit my art practice projects.

It sucks.

## So much stuff

On one hand, I've got a lot of hobbies. It's easy to get overwhelmed by the possibilities of what I can do with my time. Sometimes I just sit there wondering what hobby I should pick up and enjoy to the exclusion of actually dong it. Which is helladumb. But I can't always help it.

But also, and probably more importantly, each hobby is actually a tremendous undertaking. Let's take drawing for example. That's not just a hobby, or a job, that's an entire field. And it's only part of the equation: I want to be able to paint beautiful pictures, which is another field on top of drawing. I can say "I'm going to practice expressions. That'll make me better at drawing." But how much? It's extremely easy for me to do that, take one look at something else and go "Well, that was a complete waste of time. Drawing 10 silly faces does nothing to help me paint a beautiful landscape." And so it is with everything in my art life.

They tell you to practice what you're bad at.

What if you're bad at everything? Where do you even start?

I know I'm being a little hyperbolic there; I'm not *bad* at everything in art, but the phrase is meant in a relative way. Practice your weaknesses, rather than your strengths. If I have no real strengths, and everything is mediocre, how do I choose what to try to improve on?

## Success is its own reward

Another aspect of this problem is that because I work for myself, I rarely get feedback, and even more rarely get positive feedback, and even more rarely actually feel rewarded for the effort that I put in. Commissions are nice that way because I can get paid for doing a good job, but by the same token, they don't really reward me for honing my skills, only for the execution *of* those skills.

I guess most people don't get rewarded for refining their craft, they only get rewarded for skilled performance. Now we're back to the old [Practice vs Performance discussion](http://pencilmileage.voidspiral.com/#PracticeVsPerformance:PracticeVsPerformance%20Welcome%20TOC). The annoying thing is that you get rewarded for the "taking the test" instead of the hard part, which is "studying." But I mean, what can I do? That's the way the world works, right?

And apparently I don't hold those positive feelings from success inside myself long enough for them to fuel me. It only seems to take a few days or weeks for me to need more reassurance and feedback, even after a huge project. It's nice to go back and admire my past work (which is why I have a bunch of my sketches on the wall behind my computer in the office) but somehow that never really "fuels" me; it doesn't make me more confident in approaching a new task or project, it only reminds me that I did OK the last time.

## Aside on Art: Working for myself

I often get the sense that it's the pressure that keeps me from practicing more. I feel like I've got to do better, both because I'm doing this professionally, and because people are looking on, judging my work.

So obviously, the solution to that is for me to work purely for myself. That's what the whole sketchblog is about, right? But even then I find it very hard to just do something for me, instead of for the business, because otherwise I'm wasting time that I could be spending bettering the business, right? Even if I consider a huge pile of free-time practicing a potential benefit to the company, it's still tremendously inefficient. Inefficient enough to make me think "Well, if practicing is such a waste of my time, what I *can* do will have to be good enough." Which is also basically the impetus for me starting the [VEGI Patreon](https://www.patreon.com/voidspiral): if I can have more time to work on something, maybe I can improve the quality beyond "good enough."

## Exhaustion

Another problem I'm often dealing with is that I can only do something for so long before I get tired. Sometimes I get tired of the project, other times I get physically or mentally exhausted. Art's like that for me: I've only got about 2-4 hours of art in me each day before I'm crabby and grumpy and worn out. Some days it's better, and I'm slowly working on ergonomics solutions to some of those weariness problems, but I still find it hard to focus on an image for that long and keep making good decisions. Eventually I get lazy and start taking the easy way out, which inevitably leads to less-than-stellar images.

On a meta level, practicing for a long time on one thing does get boring for me, even if I like the process itself. I like moving my stylus, but I can only draw so many eyes or noses or hands or whatever before I'm thinking "Got this handled. Nothing new here. Just repeating myself. Bored. Want to do something else."

## What do I do?

This is a well-worn road for me. I know I've had this problem for a long time, and it hasn't much changed over the years. I'd love to stumble across **the answer** but I'm not sure that one exists.

Gladly accepting any advice, suggestions, or commentary, just hit me up on social media, links below. Or write a counterpost. Or send me a video, whatever.