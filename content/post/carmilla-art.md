---
title: "My CoS character, Carmilla"
date: 2019-01-07T10:58:49-05:00
tags: [art, tabletop-rpgs]
draft: false
featured_image: "/images/carmilla-hero.jpg"
hidehero: true
---

![A drawing of my Strahd character Carmilla](/images/carmilla.jpg)

Carmilla Czarna, my barbarian for @FluffySnowfall's Strahd game