---
title: "Antum Sketches 3"
date: 2019-01-25T10:42:38-05:00
draft: false
tags: [art, sketch, antum]
featured_image: "/images/antum-faces-1.jpg"
hidehero: true
---

![Antum face studies](/images/antum-faces-1.jpg)

Third set of tests for Antum, this time based on a bunch of stock photos. Mostly this was practice for drawing non-white people, which I'm still not great at.