---
title: Some Thoughts on Rpg Racism
date: 2020-06-26T14:56:05-05:00
tags:
- blog
- tabletop-rpgs
featured_image: ''
hidehero: false
draft: true

---
* Dnd's response
  * https://www.belloflostsouls.net/2020/06/dd-creators-are-rethinking-race-in-dungeons-and-dragons.html
  * Sidenote on my disinterest in D&D's lore
    * rules vs lore vs adventures
* ASI Lockin is the real problem
* optional race attributes
  * O2E
  * HFA
* Optional ASI bonuses for all
* Swappable ASI bonuses
* test

I recently saw [an article](https://www.belloflostsouls.net/2020/06/dd-creators-are-rethinking-race-in-dungeons-and-dragons.html) about how Wizards of the Coast is trying to clean up their act when it comes to inclusion and political correctness in their games. I'm not up on the whole situation, but I figured I'd offer my thoughts as a game designer.

You may have noticed, but D&D isn't exactly a paragon of racial equality. All told, from the wider scope of the modern world, using "races" in your game isn't a good look. On one hand, yay some for of representation I guess, but boo on a most of the rest of it. As a white male of privilege with no training in cultural studies or ethnography or sociology, I'm not the best person to break this down, but I can list a few problematic bits with D&D, and a _lot_ of other similar [#TTRPGs](/tags/tabletop-rpgs):

* Racial ability scores
* Racial traits and weaknesses
* Poorly thought out, offensive, or badly written lore

I'm sure these aren't all the issues, but they're the ones I feel I have some salient points on, so here we go.

# Racial Traits

# Bad Lore