---
title: "Ductape vs Megaphone"
date: 2019-03-07T11:55:30-05:00
tags: [blog, self]
draft: false
featured_image: ""
hidehero: true
---

*Content warning: social media, diversity*


I'm not exactly what you'd call a large publisher. I don't have a tremendous market share. I don't have the power to muscle someone out of the #ttrpg space if I tried. But I *am* white and male and incredibly lucky besides that, and I do make role-playing games. I also have a tremendous amount of #CreativeAnxiety about my work and how much I want others to enjoy it.

I read a post by someone this morning that stabbed me in the heart. I won't quote it, because I'm not trying to tear this person down, but the gist of it was this: If you're supporting white male cis-het creators, you're doing it wrong.

As someone who has a small following, when someone in my space with their own following says something like "you shouldn't support anyone in this group," and I happen to fit into that group, my mind goes all the way to "my people will defeat your people, and you will have nothing, because *no one should support you.*" Anxiety time, hoooboy!

I want diversity in games. I want diversity in my own games, and in games I own and play. I've tried to make my recent work as diverse as I can manage without undermining the concept. Granted, it doesn't always come through. Wasuremonogatari is very anime, and all the characters in the book look it. The characters in Heroines of the First Age are almost entirely women, but there aren't that many people of color in it. I'm trying harder on [#Antum](/tags/antum/), and so I'm trying to studymore to get better at drawing and painting characters with ethnicities that I'm not used to. It's not easy; I'm not confident in my abilities in the first place, so going out on a limb compounds that fear of failure. And I'd love to work with more diverse creators on my projects, but I don't usually make enough money projects to fund any creators but myself. I'm trying.

But As someone else observed this morning on Mastodon, your intentions don't matter, only your actions. The statement was kind of applying Death of the Author to social media and social interaction: If you've offended someone, they're under no obligation to hear you out if you try to correct yourself, even if you think the misinterpreted you. You have to watch your mouth, because you don't know what will offend someone.

On one hand, I agree with this sentiment. I try to do my very best in communicating with people, customers, potential customers, friends, family, critics, and haters, and everyone else. I don't do this because I think it'll get me some kind of karma, I do this because it's *the right thing to do.* I'm tired of all the negativity in the world, and if I can replace that negativity with *at least* some neutrality, if not positivity, then I'd rather do things that way.

On the other hand, I also feel like it's on us--all of us--to try to learn to cope with the world, because the world doesn't change for you, you have to change it yourself. I can't always control what offends me, but I can try to control my emotions, or let them go, or avoid that kind of stimulus. Those are things that *I* can do. And it's definitely my privilege to be in a position where I *can* avoid some of that negativity, but I also know that regardless of my race or gender I'll be confronted with things that offend me my whole life, just like everyone else.

What I'm getting at is that the issue of elevating the public discourse is something that we *all* have to participate in. As a speaker, you should try to offend less people, for the good of others. As a listener, you should try to be offended less, for your own good.* And we should all stop dehumanizing each other for things that we say. None of those suggestions place or shift blame, nor are those statements about who you are. They're moral and pragmatic suggestions.

*I know this statement is going to fly like a lead balloon, but hear me out. When someone says something that demeans you, you can be offended, but holding on to that negativity isn't fundamentally going to *help* you. Not in your actions, nor in your beliefs. This is why creators so often pass around the maxim "Don't read the comments" and why "Haters gonna hate" is even a meme. If you can find it within yourself to forgive that person, to ignore the offense, or to see the human beyond the words, maybe even *why* that person said those things, that *will* help you, and the rest of us. It'll let you release that negativity faster, help take the negativity out of your day, and if you do it long enough, maybe out of your life. This whole blog post is me trying to take that higher road.

But on the third hand, I'm really of the "don't be a dick" school of thought. Just don't. There's quite enough assholery in the world already, and it's not doing anyone any good to be needlessly mean or thoughtless to other people. It's incredible to me how angry and offensive people are these days. Absolutely astonishing. I wish I could change it. And so I'm here to offer some (probably unwanted*) advice.

*I also understand that to people who are not white men, this will inherently be another case of "the white man says do this," but as I'm trying to explain, there's nothing I can do about that "white man" part. It could just as easily be "this rando on the internet says do this."*

> ## Aside: Personhood is important to me
>
> If you look at the games I've published so far, you'll note that there aren't a lot of categorically bad enemies in any of them. Those that do exist are often phrased as forces rather than persons. I have an unconscious aversion to creating groups of people that are just categorically bad, and that might be part of the reason why the monsters *are* the characters in so many of my works.
>
> Personhood is important to me. I don't want to be the kind of person who makes sweeping generalizations about groups of people, and when I do, it makes me feel uncomfortable. I want to give people the benefit of the doubt. I want to be the best person I can, and I want to try to help others do that too.

## Taking a step back

To apply that personhood seeking to the post I saw this morning, I get the sense that this person hasn't experienced the success they desire. I feel that *whole-heartedly.* That's how I feel every day.

But here's the thing. Telling others to be *less* successful doesn't make you *more* successful. Success in an industry isn't a zero-sum game, unless the market's already completely saturated. And we don't live in a world where we get to tell others what to do with their lives. Haters gonna hate, as they say, which I take to be a universal truth.

## Megaphones, plz

Telling one group to shut up or stop succeeding is like putting duct tape over someone's mouth.

I'm recommending a different approach. Find something you like. Anything. Find the thing you'd rather have, and **hype that fucking thing.**

**Get your megaphone out, and *help* someone with it.** Tell others what you think is cool. It's okay to present it as an alternative to the thing you hate, but you don't even have to do that, since it'll just get them more views anyway.

## unsociable media

This is one of the things I struggle with online. It's way, *way* bigger someone ragging on me because I'm a white male. I see a *lot* of this negativity *everywhere*. "You shouldn't support X, because it's terrible." I mean, there are cases where that's objectively true, of course. But often we don't need to flog the dead horse as much as we do.

This culture of trading offenses is wrecking our social lives. **An eye for an eye leaves the whole world blind.** And now we're in the blind world. Good job us. Another terrible outcome from this system is that people's entire lives can get destroyed over statements they made long ago and don't agree with anymore. People *can* change, but won't want to if all you do is condemn them rather than ask them to. Now, if they stick to their guns and maintain their deplorable behavior, that's a whole other matter. Then it's pitchfork time.

I understand the value of the collective finding and revealing the wrongs, evils, crimes, and errors of popular creators, but it's also on us to not blow those things out of proportion. Being a white male game creator isn't a sin that I should be condemned for, it's not even a choice. Call me out for not doing enough to support diversity in my games, rather than calling me out because I'm a white male. One of those things I can change, the other I can't.

But I'd still rather see you get out your megaphone than the duct tape, even if I'm not the target.