---
title: "Blender to Cura Scaling"
date: 2019-12-10T11:09:55-05:00
tags: [art, software, 3d, blender, blog]
draft: false
featured_image: ""
hidehero: true
---

*A non-rant! Wow!*

I've been working on some 3d printing miniature sculpts for D&D using Blender, and since my slicer of choice is Cura, I decided to spend some time to really get to know the scaling between the two.

I've got two major problems:

1. STL doesn't contain unit information
2. Blender can't run some algorithms at the millimeter scale.

Now, the first issue is problematic because it means that slicers basically have to guess what scale the file's in. Cura does that pretty ok, and until I hit issue 2, I was fine designing assets in millimeters, exporting them to STL, and importing them into Cura. Cura would rescale them appropriately and somehow hit the magic number to get the asset to the scale it ought to be, even though there was no unit information in the file.

But then I started doing figure sculpts, such as this one:

![Imgur](https://i.imgur.com/WpMjj3S.png)

Now, that worked... ok for a bit, but I rapidly ran into the issue that the algorithm that automatically skins a mesh has some kind of minimum resolution, and just completely fails on meshes that are on the millimeter scale, ie what I'd be printing at.

So then I started re-scaling things in Blender without concern for how they'd come out in Cura, because hey, I can re-scale things to the height I want in Cura anyway, so who cares, right?

People will care. I don't want to keep all these sculpts to myself, I want to give them to others, and that means having a consistent scale and output size. So since most people work in Cura, as do I, I might as well make sure that models I make import into Cura without either needing to be re-scaled manually, or re-scaled by Cura's import process.

## Fixing the Scaling Issue

It took some trial and error, but here's what I've found works. It turned out to be way simpler than I thought it would, fortunately.

* Set the Unit Scale in the scene to 0.01.
* Set the Length unit to Millimeters.
* On export, check Selection Only, unless you actually manage your scenes right.
* Set the export Scale to 10.0.

![Imgur](https://i.imgur.com/F51nZlp.png)

![Imgur](https://i.imgur.com/tIjSM15.png)

Now, when you export that way, you should be able to import an STL exported from Blender into Cura at the correct size and 100% scale.

## So, Sculpts anyone?

I'm trying to get back into 3d, I have a 3d printer, and I'm building out a massive collection of D&D terrain and minis. Have 3d, will travel.

Each model only takes me around 3-5 hours, which doesn't seem that bad to me, considering that a decent painting from me takes longer than that. I think that this could be a viable thing. I like creating creatures, I like sculpting, and I like D&D. Match made in heaven.

If I started sharing sculpts, should I do it for free, and have a Patreon for them? Or should I sell them in a kickstarter? Or are there other places you'd sell 3d print models? Is the market too saturated? Is the product too niche?