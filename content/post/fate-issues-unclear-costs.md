---
title: "Fate Issues Unclear Costs for Success at a Cost"
date: 2019-03-11T07:15:30-05:00
tags: [tabletop-rpgs, game-design, blog, fate-core]
draft: false
featured_image: ""
hidehero: true
---

*A continuing series on the #CommonComplaints of [#FateCore](/tags/fate-core)*

This one's deceptively simple. When you roll a tie or fail and choose to succeed at a minor or major cost, it's not always clear what that cost should (or even could) be. This has happened in my games a fair amount. You can ask the PCs what they think caused the error or failure, but that doesn't always help either. Additionally, the PCs may be nervous about failure *because* they're not sure what could be at risk. That makes them feel like they're ([paradoxically](/post/fate-issues-players-have-too-much-narrative-control/)) not in control. In any case, it's always a *choice* to succeed in those cases, so if the GM offers a cost that's too great, they can just, you know, *not succeed.* Just because the option's there doesn't mean you have to use it.

I think this is also one of the things that makes Fate *seem* freeform. Sure, the game doesn't specify what you might be risking, but that doesn't fundamentally change the rules situation you find yourself in. You're choosing to scrape by through giving something up. It's not really a get-out-of-jail-free card, nor is it likely to vastly change the situation. It's a micro moment, just like the other elements of any other resolution mechanic.

I don't have much advice for "improving" this one, because I don't think there's much really wrong with it. Going into the unknown is what many [#ttrpgs](/tags/tabletop-rpgs) are all about, and so I think having some of that risk there is perfectly reasonable. It is, however, important to understand that players from more rigidly defined and highly predictable systems will be leery of this concept, particularly if they're used to their GMs or games taking advantage of them at every opportunity.