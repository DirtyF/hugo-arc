---
title: "Why PCs and NPCs Should Work Differently"
date: 2019-02-28T11:42:37-05:00
tags: [tabletop-rpgs, blog]
draft: false
featured_image: ""
hidehero: true
---

*Hint: they serve fundamentally different purposes.*

I'm writing this post sort of as a way to collect my thoughts before diving into a particular design challenge in this vein on #ATSD.


One of my biggest problems with D&D has always been encounter management. Because I prefer to run my games very seat-of-the-pants, allowing the PCs to go wherever and do whatever, I like to have the ability to invent challenges on the fly. That's why I wrote [Dr. Moreau's Monster Toolkit](https://www.patreon.com/posts/dr-moreaus-24288712), and it's why in most of the games I design I provide the GM with methods and shortcuts to create characters and adversaries while in the middle of play, instead of needing to prepare everything in advance.

I think this is very important, especially for those who follow the Sly Flourish method of game prep/running, or run with an outline like I tend to, but I also think it's important to games where you *do* have a clear plan as well, because the very nature of #ttrpgs is for the PCs to completely derail any plans the GM has for where the session & story will go. I'll have another blog post about that at some other time, but for now, consider that you're more likely to railroad your players if you have no way to "follow" them through the story, i.e. you can't invent challenges on the fly to keep up with them.

Consider the difference between a skill check and an encounter in D&D, for example. If your players go "off the rails" you can come up with skill checks all freaking day, but it'll take you *at least* a few minutes of distraction and story derailment to invent a new encounter. In some games, it's better than others, of course.

I think one of the primary culprits--if not *the fundamental one*--is that a lot of games treat NPCs exactly or almost exactly like PCs. While I admire this from a system design perspective, in that yeah, "creature" is a class of object, and PCs and NPCs are both members of that class, and therefore it makes sense for them to share the same attributes, I dislike it from the perspective of *work*. If an NPC is fundamentally the same stuff as a PC, then you're immediately faced with generally doing as much work as all the Players combined: they each only have one character to control & develop, whereas you'll often have that many in a *single combat encounter.* 

Now, I'm not saying that D&D *doesn't* do this, monsters don't have levels or classes, after all, but it's also not avoiding the problems either. You still need a ton of statistics that basically mirror a PC exactly.

## Form Follows Function

PCs and NPCs have different purposes in the context of the game. PCs exist to be the player's avatar in the game world, and to enable them to take actions and construct their narrative how they want. NPCs exist to oppose, assist, or provide a foil for the PCs, as well as provide narrative tools to the GM.

It follows, therefore, that PCs and NPCs need not have all the same statistics, or have anywhere near the same level of detail. I'd even go so far to argue that a well-designed system should let the GM ascribe more detail to an NPC as they become more important; a kind of just-in-time development, if you will. That way, you can introduce any kind of new NPC quickly or even instantly, and only give them the information and statistics that you need at the time. You can then "upgrade" them as you go, *if* they happen to become more important, either by turning out to play a larger part in the story, or by coming into conflict with the PCs. If an NPC is a shopkeep, he doesn't need hit points unless your PCs decide to try to kill him, for example.

## Soft Difference

Soft Difference is when the system assigns mostly the same kinds of stats to NPCs but changes how you arrive at those stats. An example of this would be D&D 3e, 3.5e, and 5e. Especially in the case of D&D, I'd argue that it's easy to mess this up. It's easy to give a *different* but not *less complicated* way for arriving at those stats, as D&D tends to do. The monster-creation rules for 5th ed are 10 pages, after all, and the "quick" method still has circular effects in it.

A more ideal version of the 5th ed quick creation system would do away with the secondary recalculation of CR, but also have more options. (Which is basically what I did with #DMMT)

## Hard Difference

Hard Difference is when the stats the NPCs have aren't directly tied to the stats the PCs have *except* where those stats would interact directly. An example of this would be Harm in Apocalypse World or other PbtA games. NPCs don't have moves the way PCs do, but they *do* have health and harm values which parallel PC stats.

A lot of the time it seems that this will boil down to stripping out statistics that only marginally make a difference to the experience of encountering that creature. Other times it comes down to unifying statistics and using reasonable guesses, though this can feel too loosey-goosey for some systems, especially if the rest of the system is very precise and specific.

## But Not TOO Different 

Obviously, it won't do anyone any good to throw the baby out with the bath water. Certain statistics *will* need to be present, and figuring out what those are is key to figuring out what the "minimum viable monster" is. If you can figure out what stats you *absolutely must have,* then you map outwards from there for details you can add to the NPC when they become necessary.

## Building a Table

[I like tables.](/post/why-i-love-analytical-game-design/) Tables are fast. If you've got a table for creating NPCs, you're a good way there. (So long as it doesn't end up requiring you to make changes, then refer to the table all over again to figure out what you did, then do that over again to correct for your adjustments.)

Your table should include your required stats, but should also include any of the optional stats as well, organized by importance, if possible. The major axis should probably be either "power" or "level," but it could also be "importance" depending on the range of power levels in your game system. A narrative game, for example, might use "Minor, Supporting, and Major" levels. 

## Reductive & Additive Strategies.

> Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away.
> 
> -- Antoine de Saint Exupéry

Your job, in making your game system, is to make it easy and fun to use and play. For that, you must trim away stuff that's not important, such as the little fiddly bits that make NPC creation take forever. Give them options that can be safely ignored, rather than a huge group of decisions that all must be made to complete the task.

Your job, as a GM, is to make the game enjoyable and memorable for all. That's a lot easier to do when you can follow the PCs and present appropriate challenges to them when the need arises, rather than pre-planning or post-planning.

Now go forth, and make it easier for GMs to create NPCs at the drop of a hat.