---
title: "Watching Myself on Loop"
date: 2019-03-18T07:39:11-05:00
tags: [blog, art]
draft: false
featured_image: "/images/Anastacia-hero.jpg"
hidehero: true
---

*The weird experience of feeling comptetent for once*

Last week I finished a cool [commission](https://gitlab.com/voidspiral/FreelanceProjects/wikis/commissions).

![Fire Priestess Illustration](/images/Anastacia.jpg)

The client loved it and left a glowing review, and the community loved it too, at the time of this writing it's my highest rated post on my [Pixelfed.Social Sketchblog](https://pixelfed.social/Arcandio). That's awesome.

Then I made a timelapse video, sort of to try out using Kdenlive and compare it to Resolve. It didn't take long to make the video and I had it uploaded to [Youtube](https://www.youtube.com/watch?v=gt9-KOBqqug) later that day:

<iframe width="560" height="315" src="https://www.youtube.com/embed/gt9-KOBqqug" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## What's weird about it

I consider this state of affairs notable for the following reasons.

* I can't stop watching the video. I keep coming back to it to watch the evolution of the piece all over again. I've got no guesses as to why I keep watching it.
* It's nice that I can watch an art video without that video triggering my inferiority complex and #creativeanxiety
* It reminds me that I can do cool stuff, occasionally, but it's also sort of dissociative: I watch it like "ok, I see how this artist works, that makes sense." Which feels... odd.
* It subverts my lack of confidence, which is nice, but hard to keep in my brain.

## Why I keep sketches up on my walls

I'm not going to post a photo at the moment because a lot of the stuff that's currently up is drawn heavily from reference, but this is sort of why I try to keep some of my art on the walls now. It reminds me of what I *can* do rather than what I can't.

I do wonder if there's a place in my workflow for this kind of video. Whenever I stream, I save the video as well, so it's not that much harder to compile the video after the fact. The main problem on that end is that it's less straining on me to work without streaming, because I feel like I can take more breaks and not bore people possibly watching the stream.

The other problem I see with these is that I'd really like to be talking over the top of them, but I can't think of anything I'd want to say about them. The process is fairly self-explanatory, and it's pretty much the same process every single time, so that'd get boring for me to talk about and for others to listen to. And if I were to talk about something else, what? I feel like I'll just make an ass of myself if I try to talk about art topics like drawing, refining, coloring, etc despite the fact that I'm working on an [art wiki](http://wiki.arcandio.com/). And most of these are commissions, so I can't just yutz about the character, because I know nothing about them.

Any ideas? Hit me up on social media.