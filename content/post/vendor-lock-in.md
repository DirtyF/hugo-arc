---
title: "Vendor Lock In"
date: 2019-03-24T10:24:32-05:00
tags: [blog, rant, software]
draft: false
featured_image: ""
hidehero: true
---

*A brief rant on software selection & data control.*

I hate vendor lock-in. With the passion of a thousand suns. I've literally built my entire business model around avoiding vendor lock-in and maintaining data integrity. As a creative, I just can't afford to let someone else dictate how I use my data, because my data is my job.

[I've been off the Adobe train for a long while.](/post/acrobat-rage/) I got out on CS6 right before the rental pay structure, and I've been extremely happy with that state of affairs. I've used CS6 since then when I need it, and to be fair, that's a lot sometimes, working on RPG books, but I haven't paid them anything this whole time, for like 6 years or something. Thankfully, their license servers have still worked, but that's not guaranteed. That could evaporate too, like CS2 support. And it likely will.

So I hedge my bets. I try to stay as flexible as I can and make use of enough open source software as possible. I spend time figuring out alternate workflows. I love Clip Studio Paint more than any app I use, but I could get by if I had to without it. I'd hate it, but I could.

There are some things that are much more deeply integrated into my pipeline and life: Amazon and Google. I'm not happy with this state of affairs, but there isn't much I can do about it at this point. The google ecosystem is too useful to me, and amazon allows me to get products for myself and for the business at prices that just annihilate any concept of price competition.

But there's another reason I'm trying to break with big corporations: I support the little guy. Big businesses don't innovate, they iterate. They're too invested in the status quo (semantically and financially) to take risks, and so they stagnate. Take a look at almost any old codebase. The older the software, the more bloated and clunky it is. You *need* little guys to create new projects, because they're the ones changing the way things work and forcing competition on their larger brethren.

So I'm going to try to support the little guy. I hope you do too.