---
title: "Why I Design Games When I'm Sick"
date: 2019-02-22T11:04:40-05:00
tags: [blog, game-design, tabletop-rpgs]
draft: false
featured_image: ""
hidehero: true
---


> Short one today. Topical though, since I'm still feeling like shit. As the saying goes, I'm sick and tired of being sick and tired.

As you probably know, I have a company [Voidspiral Entertainment](https://voidspiral.com). That's what I do full-time. I'm not doing this as a hobby, I'm trying to do it to help support the family. It's dicey, but that's what I want to do.

I can get stuck in projects for a long time, accidentally working long hours, forgetting lunch, and working on the weekends and evenings when I'm really deep in a project. As they say, when you own your own business or work from home, it's hard to leave work at work. It also doesn't help that I come up with a fairly solid new game idea about once a freaking week, so there's never enough time to do everything I want to do.

I don't get sick that often. Like any other human, I have days where I think ahead to the tasks I need to do and don't even feel like rolling out of bed, but fortunately, those days are rare. Even game design can be a chore sometimes, especially if you're also doing the publishing, marketing, and finances too.

I've also got a *ton* of [hobbies](http://josephleebush.com), including astronomy, archery, hiking, biking, camping, ontology, and even occasionally playing games. So when I get sick, you'd expect that I have plenty to do.

But nevertheless, I often find that somehow I don't. I think it's the overpowering work ethic of home business that if I'm not working, the work ain't gettin done. But also, I just feel guilty when I'm not working. Taking a day off intentionally is one thing (since we don't do that very often) but when I'm sick I end up in a spiral about "Oh I should be doing this or that," then "I can't because I can't even think straight," then "what am I doing just lazing around? I should find something to do," then "that wasn't productive, I should be working," and back around again.

So what often happens is that I either end up on, or end up *starting* a different project. There are a few benefits to this:

* Since it's not a main-line thing, I can put it down and relax and still feel like I did something productive.
* It allows me to put my weary, confused mental state to work coming up with new ideas.
* It's way less boring than sitting around twiddling my thumbs.
* It's usually so ... what's the word ... _absorbing_* that I can spend hours on it and not really think about how bad I feel.
* It's fun. It's easy to fall into the trap of feeling like the current project is just dragging on forever, which makes long-term projects feel unfun after a while.
* Because I'm not dwelling on "being sick" I don't overthink being sick and weaken my immune system with depression

*I literally can't think of words, which is one of the problems. I spend a lot of time using Power Thesaurus to figure out what some word is.*

Additionally:

* It doesn't always help me actually get better, since I'll stay up and not get all the rest I probably need.
* While starting a new project or working on a side project isn't *wasting* time, it's not as productive as just working on the main project.
  * I feel like this is also related to #TheWillpowerGame: when I'm sick I've got very little willpower, and so it's easy to go do something else rather than "work" work.
* I still get distracted a lot, which means the project I'm working on can be kind of scattered or incoherent when I come back to it.

So that's it. Why I'm sitting here, taking a break from working on #ATSD to write a blog post about why I was working on ATSD while sick.