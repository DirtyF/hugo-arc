---
title: "Some Thoughts on Talent"
date: 2019-03-29T10:49:44-05:00
tags: [art, self, blog]
draft: false
featured_image: ""
hidehero: true
---

*I had a fantastic talk with NordicForge and Caconym on my Twitch art stream this morning that prompted some more detailed thinking.*

Being called talented is one of the more frequent ways I receive complements, when I get them. I have complex feelings about this. Firstly, I appreciate the compliment. Absolutely. With all the creative anxiety I experience, every time someone says that they like my work, it fills me with pride and happiness and energy. But here's the thing. I'd much rather be called "skilled" or "good" than "talented."

I don't think I'm talented. If there are consequences for being skilled but not talented, I'll probably experience them at some point, if I haven't already.

## Definitions

Since this topic is mostly about the *meaning* of talent, let's get that out of the way.

Talent is defined as:

> A marked natural [ability](https://en.wiktionary.org/wiki/ability) or [skill](https://en.wiktionary.org/wiki/skill).
>
> -- [Wiktionary](https://en.wiktionary.org/wiki/talent#Noun)

But Sycra puts up some good speculation in the first third of this video (warning: long video. You may forget about this post, and that's ok.)

<iframe width="560" height="315" src="https://www.youtube.com/embed/QPY7d23fScQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The comments are also very interesting as well, check those out too: https://www.youtube.com/watch?v=QPY7d23fScQ

## What bugs me about talent

* I feel a bit insulted when someone says that I'm "talented."
    * In some cases, this is because they're phrasing it like "Oh, well, you're *just* talented." Which obviously burns me up because it ignores all the hard work I put in, not just growing up, but in school, at work, and on a daily freaking basis.
    * In other cases, since this person who's calling *me* talented is  implying that they'll never be able to draw like me, that *I* in turn will never be able to draw like the artists that *I* idolize. The focus on talent becomes an **excuse** for not pushing yourself to get better.
* Sure, you might mean that I'm just good at drawing, but few if any of the words you might replace "talent" with have the same connotation of *natural, innate* ability. Use skill instead. Or good.
* Talent is rarely applied to masters. You don't really say that Rembrandt was "talented." You might say he was "great" or "expert" or "adept" or whatever, but to call him (or another master) "talented" is just sort of... banal. Like saying that he was "proficient." Or "Apt." Which is weird, because the word itself means "outstanding aptitude," but it still seems to remove the devotion and obsession masters show to their craft.
* Someone who's talented but untrained only knows a specific subset or genre of skill, likely the thing they're most interested or exposed to.
* I've often felt like my own talent was holding me back. People tell me that I'm good at something, so it tricks me into complacency. Good enough is the enemy of greatness.
* Completely naive and untrained talent seems to contradict the four stages of competence. Which is more likely, that someone's practiced a lot or studied a lot without remembering or knowing it, or that they were born with intrinsic knowledge and skills that were latent and dormant until awakened? Since it's hard to know the life-long habits of anyone we meet, it's easy to think that they've just always been that good, or that they never did anything to "earn" their skill, when the opposite is probably true.
* A lot of people get called talented when they're young, and I think that this is usually one of two things: they're better than their peers, which will necessarily always happen in a given population, and/or they've been practicing more than others. A lot of good young artists get good because they naively copy things they like, over and over again. Are they more talented? No, they're more practiced.

## My real-life talent mechanic

*Leave it to a game designer to try to analyze a real-life phenomenon by ascribing it game-like rules.*

I propose this way of thinking about it.

1. Talent and practice are two inseparable halves of "skill." You can't have one without the other.
    * From now on when I say "talent" I mean "natural talent" as compared with practice and training.
    * The meaning of practice is easily understood here.
    * In talent I lump things like "having a good eye," but also *having eyes at all* and hands or feet you can draw with. In that way, we can liken talent in art to talent for sports: there's a physical component that *enables* the activity. Some people might have better or worse physical components: an painter with terrible eyesight or an NCAA player who's 5'2" and weighs 90 lbs soaking wet.
    * Art is *mostly* a mental game, however. Manual dexterity is far less important than how you think and approach the artmaking process.
    * In the absence of *either* talent *or* practice, you're not going to well at the thing in question. In this case, art. Or for the sake of argument, more specifically, drawing. If you have absolutely no talent (which according to this definition would mean no way to manipulate a drawing tool, not just that you can only draw stick-figures) then yeah, it's going to be hard to draw a "good" picture. Conversely, if you have absolutely no practice, then, likewise, you'll have a hard time drawing a good picture.
    * Both talent & practice are present *at some level* in most people. Ergo we can say that most people have *some level* of skill in drawing.
    * If it can be trained, it's not talent, it's practice. This axiom massively reduces the role that talent plays.
2. Talent and practice *are not additive.* **They are multiplicative.**
    * You can have all the talent in the world, but not be able to employ it if you have no practice. Conversely, if you have absolutely no talent for something, you can't even practice because you're lacking some physical component to the activity.
    * The more talent you have, *the more you get out of your practice.* But since just about everyone has some bit of talent, **what really matters is the amount of practice you put in.** It may be harder for you to become as good as someone else, but you *can* do it. The difference is in time. Talent is just a modifier for how quickly you improve.
3. Practice includes study.
    * No matter how many years, no matter how many thousands of drawings, you won't improve if you don't seek out new techniques, skills, and knowledge to expand your abilities. This applies equally to those who are "talented" and those who "work hard."
    * Copying is okay. Copying is good. What's bad is plagiarism. Copy from multiple sources, remix, and re-envision and you'll be in the green. Directed copying is called "studying," and if you're not doing it, you'll fall behind. To be clear though, you have to study deeply, and you have to study broadly. You can't focus to the exclusion of all other things on, for example, drawing anime faces, because then when you go to draw an anime body, you've got no experience there. Likewise, if you only study Dragon Ball style faces, you won't know how to draw Vampire Hunter D style faces.
    * Knowing how to study right is *also* a force multiplier like talent. If you can buckle down, focus, and really get in there and *learn* a new technique, you'll improve far quicker than someone who's talented but doesn't practice. But learning is a skill in its own right.

I believe it's a numbers game, like everything else in life. If you put the time in, you'll improve. If you don't, you won't. Now, wishing it was *easier?* That's the real problem.

In closing, here's a quote.

> Talent is a pursued interest. In other words, anything that you're willing to practice, you can do.
>
> -- Bob Freaking Ross

The question is whether you're willing to practice.