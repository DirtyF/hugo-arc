---
title: "Assaj Complete"
date: 2020-06-12T08:05:07-05:00
tags: [art, blog, portfolio]
draft: false
featured_image: "/images/assaj-final-hero.jpg"
hidehero: true
---

![Lizardfolk bellydancer](/images/assaj-final.jpg)

I could have done better on the shading on the snakes, but I was thrown off by how unnaturally they were colored. It put me in out of the mindset of trying to nail a true-to-life rendering style. The micro-scale texture seems to have worked out ok though.