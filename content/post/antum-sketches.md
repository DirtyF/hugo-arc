---
title: "Antum Sketches"
date: 2019-01-24T07:52:16-05:00
draft: false
tags: [sketch, art, antum]
featured_image: "/images/antum-pulp-test.jpg"
hidehero: true
---

![Antum pulp art style tests](/images/antum-pulp-test.jpg)

My first set of sketches for testing the art style concepts for Antum.