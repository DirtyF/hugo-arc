---
title: "Antum Sketches 2"
date: 2019-01-25T10:42:38-05:00
draft: false
tags: [art, sketch, antum]
featured_image: "/images/antum-pulp-test-2.jpg"
hidehero: true
---

Second set of sketches for Antum. I had some actual reference images to work from this time to help me define the style. Can you tell?