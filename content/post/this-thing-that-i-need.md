---
title: "This Thing That I Need"
date: 2019-04-10T11:02:34-05:00
tags: [blog, software, rant]
draft: false
featured_image: ""
hidehero: true
---

*This blog is rapidly devolving into a series of rants. I'm sorry.*

I do a lot of work in what you could (generously) call "thought design," both for work and for fun, and for stuff in between. I like architecting information. I've had a number of wikis for years, and I routinely use personal wikis and similar kinds of information organization tools for a lot of my day to day work and hobby time. Basically, I like to use applied information theory and ontology for everything.

So you'd think that I'd have a good system for it.

But I really, really don't.

* Scrivener is great, and I use it for almost all of my long-form writing. But it's not really the right tool for heavily linked wiki-type things. Not to mention that it uses RTF behind the scenes, so the exported data can be just garbage.
* Tiddlywiki is nice, and it's very extensible (I've written some very complex tree-viewer scripts for browsing worldbuilding wikis). But its also strict in some ways. I often found it difficult to get it to do *exactly* what I wanted, such as if I wanted to export my data to markdown or something.
* Dokuwiki is pretty good, but I don't like having to host it, either locally or remotely. Additionally, because each instance requires installation, it's not as easy to spin up a new wiki for a new idea or project, by which time I've lost my train of thought.
* Zim was ok, but I hated needing to write out the entire path of a link for links that were not directly up or down the tree from the node that I was writing from. I really liked
* CherryTree is good and fixes the namespace issues with Zim, but the data format is wacky. I *could* code up a parser, but it'd take some time.
* TheBrain expensive. Not sure if any exporting is even possible.
* Realm Works is pretty cool, but the editing UI is just too f-ing slow to be useful to me for reasonable sized projects. It also enforces structure that I don't really need, and editing that structure seems like a permanently destructive process. Additionally, export isn't super well supported.

And here's the real problem. They're all (except TheBrain) missing one feature that I *really* want: **link context.**

In a wiki, you can have node `Alice` link to node `Bob`, which is fine. You get the context from the sentence that link appears in. But what if I want to run some kind of parser that *understands* that link? It doesn't know what's in the sentence or how to interpret it, it just knows `Alice` → `Bob`. But in TheBrain (and in ontology, and in *HTML itself*) that relationship itself has properties. You could have something like `Alice` → `is the parent of` → `bob` or `Alice` → `hates` → `bob`. *That* I could parse all day. I could analyze stories and map worlds *so good* with that.

So far, I haven't found a tool that matches all these criteria, and I've tried some pretty crazy stuff.

## Use cases

* worldbuilding / setting bibles
* campaign design
* campaign *running*
* story design
* personal skill wikis

## Requirements

These are the things that prevent me from using other *preexisting* tools instead and saving myself a lot of time and hassle.

* **Fast UI.** I refuse to write a tremendous amount of information in something that I have to submit every line manually.
* **No data lock-in.** Preferably a simple and easily parsed data format that I can script later.
* **Simple linking.** Auto-applied or CamelCase preferred, but `[[internal links]]` accepted.
* **Easy startup.** No hosting. No databases, unless export is scriptable.
* **Link Context.** Sort of optional. If I had a piece of sofrware that did all the above, I'd be using that instead of worrying over contextual links.

If you know of a thing that matches those requirements, hit me up on mastodon or twitter, please.

# The Hypothetical Thing

I call the this hypothetical tool "Creative Ontology Editor," because that's what it is. It's an information design tool for creative endeavors. It borrows information theory structures and applies them to non-scientific non-enterprise data. A personal information theory tool, the same way Zim is a personal wiki.

## Key features

* quick and easy to edit, like a spreadsheet
* auto-linking between entities
* user-defined entity fields
* project-global entity fields
* minimalist interaction design
* exportable data, preferably in markdown
* configurable entity list view
* special data views
    * entity list view
    * connection view
    * timeline view
    * hierarchy view
    * single entity data field view
    * 2d / map view

## Build It And They Will Come

> I mean, probably not, or someone else would have built it already, right?

So I tried to start building the thing. And here comes the rant.

How do you write a cross-platform desktop app? I've found a number of options, and none of them have worked out for me very well. They include Python, C# .net/mono, and Node.js.

Node.js is the frontrunner at this point, because it's so closely tied to a functioning, useful, extendable UI framework. It's pretty cool, in case you haven't heard of it, it's called HTML.

Node allows you to write servers and create single-page applications, but it also allows you to write desktop apps using one of a number of packages. The one that I'm most familiar with is Electron, but there's also Nodewebkit and others.

But here's the problem with that (aside from the fact that every minute I spend coding, debugging, and configuring this beast is a minute I'm not working on "real" work): node.js is freaking hard to build. Writing the code is easy, after I really grokked React (another module on top of Node.js) I was able to get a working prototype of this app in a few weeks of afternoon work. Which is spectacular, and I'd really like it if things had kept going that well. Unfortunately, to set that up I had to use the `create-react-app` module (are you seeing a pattern here?) which explicitly prevents you from writing to the disk or reading files, for security reasons. The thing is that CRA and Electron don't get along so well because of that pattern. In theory, you should be able to use any UI toolkit or framework--React included--inside an Electron app. But by using CRA to set up Babel and Webpack (ugh more modules) and manage them without my interference, I set up my project in such a way that fundamentally breaks parts of Electron.

Now, I expect that I could fix this problem if I rebuilt the entire app backwards, starting with Electron, then adding in React manually (and possibly configuring Webpack and Babel *manually* dear god) but I'm just tired man. I thought I could make this work. I went out of the way to stop coding new features to make sure that I could build the app to production fairly early, and here we are with me needing to *yet again* start basically from scratch. Which leaves me in the same limbo trap as before:

Do I take the time to work on a tool to enable me to do my job better? Or do I just do my job worse instead?