---
title: "Fate Issues: Complex Resolution Mechanic"
date: 2019-03-06T11:52:15-05:00
tags: [tabletop-rpgs, blog, fate-core, game-design]
draft: false
featured_image: ""
hidehero: true
---

*A continuing series on the #CommonComplaints of [#FateCore](/tags/fate-core/)*

Now this is a hard one, because it can be true. But at the same time (at the risk of being called out for "whataboutism") D&D and other systems often have identical mechanical events that just aren't phrased the same way.

The basic problem goes like this: The core resolution mechanic of Fate Core has too many steps to determine success or failure:

1. pick a skill
2. pick an action type
3. GM picks TN
4. roll your dice `4dF+[stat]`
    * optional: spend free invokes for a bonus
    * check for success
5. optional: pay a fate point to reroll
    * check for success
6. optional: invoke an aspect for a bonus
    * check for success
    * possibly repeat
7. check for success: determine whether it was a fail, tie, success, or success with style
8. apply the effects of the action's outcome (ie deal damage or gain an aspect etc)

If you diagram it out and compare it to others, you get something like this:

[![a diagram of resolution mechanics](https://i.imgur.com/EVYJGVp.png)](https://i.imgur.com/EVYJGVp.png)

(not perfect, of course. Plenty of problems with the diagram, so don't take it as gospel)

First off, **yes, that can get complicated.** Here are the buts:

1. The complicating factors are optional.
2. The complications are only likely when someone *really* wants to succeed at something and is willing to sacrifice a lot for it.
3. You can spend a lot those points in advance too, if you *know* you really need to succeed in advance.
4. Once you've memorized those optional elements, you don't have to bring them up in *every single* roll you make.

The other part of this is that it seems too cyclical for some players. Some players just want to roll the dice and know immediately whether they've succeeded or failed. Fate, as written, doesn't do that. It allows you more control over your success or failure, by replacing the question of "are you both lucky and skilled enough to do the thing" with "what would you give to succeed at this thing?" And to be fair, there are plenty of other cycles in other game systems, especially in D&D, where an attack roll leads to a damage roll, then calculation of resistances & immunites & weaknesses, then possibly triggers other effects, which in turn *may trigger other effects.* PBTA makes a better comparison, because it generally limits or avoids any contingent abilities, except perhaps for exchanging harm, and there's no way to retroactively (or even proactively, in a lot of cases) modify your roll to get a different result.

Now, when I've designed Fate games, I do sometimes modify the resolution mechanic a bit. I may for example remove the retroactive capability: all invokes and bonuses have to be declared before the dice hit the table. This makes it feel more like bidding than wresting control. Other times I may remove DaSD for a more "traditional" arrangement of narrative control.

The other thing I'd like to note here is that I do like the idea that the mechanic "expands" or slows down when the check is important. That's a neat idea: that the more important the roll, the more time can be spent determining its success. This can be seen as a good thing because it introduces an element of *suspense* that a fast, simple, one-and-done roll mechanic rarely displays. Think of those micro-moments in a D&D game where your barbarian is taking a few seconds to determine how good their attack roll against the BBEG is. There's a certain element of suspense to that, especially as they desperately calculate their damage--and then they realize an error and recalculate--before you find out how that damage did against the villain. Those micro moments of suspense **are valuable** and they're often overlooked. With Fate Core's resolution mechanic, there's a negotiation over the success of the action, and if that action is important, the negotiation will probably take longer as the player looks for more advantages to employ, which in turn builds suspense.

The problem with that is that it *can* happen too often, especially with players who are so aggressive that they *must absolutely* succeed on *every single roll.* That's not how the game is designed to work, it's designed to work best when there's a give and take of success and failure.