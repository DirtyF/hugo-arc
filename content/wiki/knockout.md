---
title: "Knockout"
date: 2020-06-12T07:56:56-05:00
tags: [wiki, art]
draft: false
featured_image: ""
hidehero: true
---

Knockout is an technique where you keep lineart in your image, but instead of leaving the lineart all one color (usually black) you colorize certain portions of it. As far as I've seen, this is usually done to reduce the [line weight](/wiki/line-weight) of the lines by making them similar in [hue](/wiki/hue) and [value](/wiki/value) to the surface they relate to.

An example would be coloring the lineart of an uncovered arm to be dark red-brown, while the green shirt above it has a dark green or black line.

