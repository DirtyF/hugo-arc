---
title: "Sketch"
color: "#f99"
featured_image: "/images/assaj-sketch-hero.png"
---

Artwork that either isn't finished, or isn't *intended* to be finished.