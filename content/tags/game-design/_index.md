---
title: "Game Design"
color: "#f99"
featured_image: "/images/some-voidspiral-products.jpg"
---

Articles about my experiences, philosophy, and methodology in game design.